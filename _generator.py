import random


def get_random_password():
    password = ''
    for i in range(8):
        password += random.choice('qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890@#!?$%')
    return password

def get_random_username():
    usernames = ['Naruto', 'Sasuke', 'Orochimaru', 'Itachi', 'Madara', 'Obito', 'Kakashi', 'Minato', 'Kushina', 'Hiruzen', 'Tsunade', 'Hashirama', 'Tobirama', 'Gaara', 'Sasori', 'Nagato', 'Yahiko', 'Konan', 'Hinata', 'Sakura', 'Ino', 'Shino', 'Kiba', 'Shiikomaru', 'Gai', 'Rin', 'Kira Bi', 'Boruto', 'Sarada', 'Suj-getsu', 'Karin', 'Kimimaro', 'Kaneki', 'Ayato', 'Touka', 'Hideyoshi', 'Jason', 'Sasaki', 'Arata', 'Nine', 'Twelve', 'Yagami', 'L', 'Momoshiki', 'Urashiki', 'Kaguya', 'Rudeus', 'Silfietta', 'Roxi', 'Eris', 'Shishui', 'Kagami', 'Fugaku', 'Mikoto', 'Haruha', 'Midariima', 'Kuroko', 'Aomine', 'Taiga', 'Riko', 'Miisaka', 'Kaori', 'Madoka', 'Jabami', 'Ryouta', 'Saotome', 'Sorata', 'Jin', 'Mashiro', 'Misaki', 'Nanami', 'Arima', 'Suzuya', 'Temari']
    username = random.choice(usernames)
    usernames.remove(username)
    return username